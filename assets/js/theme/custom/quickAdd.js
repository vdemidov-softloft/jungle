export default function () {
    const action = document.querySelector('#quickAdd');
    let formAction = '';
    const sku = document.querySelector('.input-sku');
    const qty = document.querySelector('.input-quantity');
    const ref = document.querySelector('.hide-link');
    const button = document.querySelector('.quick-add-button');

    action.onkeyup = () => {
        const skuVal = sku.value;
        const qtyVal = qty.value;
        formAction = `/cart.php?action=add&sku=${skuVal}&qty=${qtyVal}`;
        ref.href = formAction;
    };

    button.onclick = () => {
        if (sku.value.length > 0 && qty.value.length > 0) {
            ref.click();
        }
    }
};
