export default function () {
    const buttonList = document.querySelectorAll('.see-you');
    if (window.innerWidth < 800) {
        buttonList.forEach(but => {
            // eslint-disable-next-line no-param-reassign
            but.onclick = () => {
                const marker = but.querySelector('.marker');
                const show = document.querySelectorAll('.show__this');
                const hide = document.querySelectorAll('.hide-marker');
                if (!but.nextElementSibling.classList.contains('show__this')) {
                    show.forEach(elem => {
                        elem.classList.remove('show__this');
                    });
                    hide.forEach(elem => {
                        elem.classList.remove('hide-marker');
                    });
                }
                marker.classList.toggle('hide-marker');
                but.nextElementSibling.classList.toggle('show__this');
            };
        });
    }
}