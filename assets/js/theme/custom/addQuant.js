export default function () {
    const quant = document.querySelectorAll('.input-quat');
    quant.forEach(elem => {
        // eslint-disable-next-line no-param-reassign
        elem.onkeyup = (e) => {
            const father = elem.closest('.bot-box');
            const addCart = father.querySelector('.add-card a');
            const howMuch = e.target.value;
            const hrefString = `${addCart.href}&qty=${howMuch}`;
            addCart.href = hrefString;
        }
    });
}